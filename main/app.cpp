/*
 * Created on Thu Mar 12 2020
 *
 * Copyright (c) 2020 Peterson Yuhala, IIUN
 */

#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <thread>

#include "app.h"

/* Darknet variables */
static data training_data, test_data;

/* Benchmarking */
//#define BENCHMARKING
#include "benchtools.h"
#include <time.h>
struct timespec start, stop;
double diff;
bench_info *bench_point;
std::vector<double> sup_times;
std::vector<double> chkpt_times;
std::vector<double> loss;
std::vector<int> epoch;

/* For benchmarking */
void start_clock()
{
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
}
void stop_clock()
{
    clock_gettime(CLOCK_MONOTONIC_RAW, &stop);
    //printf("Ocall time: %f\n", time_diff(&start, &stop, MILLI));
    if (bench_point->chkpt == 1)
    {
        chkpt_times.push_back(time_diff(&start, &stop, MILLI));
    }
    else
    {
        sup_times.push_back(time_diff(&start, &stop, MILLI));
    }
}
//Results files
std::ofstream file;

void add_loss()
{
#ifdef CRASH_TEST
    file << bench_point->epoch << "," << bench_point->loss << "\n";
    file.flush();
#else
    loss.push_back(bench_point->loss);
    epoch.push_back(bench_point->epoch);
#endif
}

void crash_test()
{
#ifdef CRASH_TEST
    file.open(CRASH);
#else

#endif
    bench_point = (bench_info *)malloc(sizeof(bench_info));
    std::string img_path = MNIST_TRAIN_IMAGES;
    std::string label_path = MNIST_TRAIN_LABELS;
    training_data = load_mnist_images(img_path);
    training_data.y = load_mnist_labels(label_path);

    char base[] = CONFIG_BASE;
    char cfg[128];
    snprintf(cfg, sizeof(cfg), "%smnist.cfg", base);
    train_mnist(cfg);

#ifdef CRASH_TEST
    file.close();
#else
    //Results files
    std::ofstream crashFile;
    crashFile.open(NOCRASH);
    crashFile << "epoch,loss\n";
    crashFile.flush();
    for (int i = 0; i < loss.size(); i++)
    {
        crashFile << epoch.at(i) << "," << loss.at(i) << "\n";
        crashFile.flush();
    }

    crashFile.close();
#endif
}

void run_bench()
{

    bench_point = (bench_info *)malloc(sizeof(bench_info));
    std::string img_path = MNIST_TRAIN_IMAGES;
    std::string label_path = MNIST_TRAIN_LABELS;
    training_data = load_mnist_images(img_path);
    training_data.y = load_mnist_labels(label_path);
    //Results files
    std::ofstream saveFile;
    std::ofstream restFile;
    //write headers of results files
    saveFile.open(SAVE, std::ios::app);
    restFile.open(RESTORE, std::ios::app);
    saveFile << "model_size(MB),sup_save(ms),chkpt_save(ms)\n";
    //saveFile << 0 << "," << 0 << "," << 0 << "\n";
    saveFile.flush();
    restFile << "model_size(MB),sup_restore(ms),chkpt_restore(ms)\n";
    //restFile << 0 << "," << 0 << "," << 0 << "\n";
    restFile.flush();
    printf("Benchmark progress: 0%%\n");

    for (int i = 1; i <= 50; i++)
    { //we have 10 config files in total
        //choose config file
        char base[] = CONFIG_BASE;
        char cfg[128];
        snprintf(cfg, sizeof(cfg), "%scfg%d.cfg", base, i);
        //printf("cfg: %s\n", cfg);

        //part a: bench param saving
        bench_point->chkpt = 0;
        bench_point->sup = 0;
        bench_point->model_size = 0;

        printf("Save bench: \n");
        train_mnist(cfg);
        //calculate mean/median of points in vectors here and write to save file
        std::cout << "Save point: " << bench_point->model_size << "," << get_median(sup_times) << "," << get_median(chkpt_times) << "\n";
        // saveFile << bench_point->model_size << "," << get_median(sup_times) << "," << get_median(chkpt_times) << "\n";
        saveFile << bench_point->model_size << "," << get_mean(sup_times) << "," << get_mean(chkpt_times) << "\n";
        saveFile.flush();

        //clear vectors
        chkpt_times.clear();
        sup_times.clear();

        //part b: bench param restoration
        bench_point->chkpt = 0;
        bench_point->sup = 0;
        bench_point->model_size = 0;

        printf("Restore bench: \n");
        train_mnist(cfg);
        //calculate mean/median of points in vectors here and write to restore file
        //restFile << bench_point->model_size << "," << get_median(sup_times) << "," << get_median(chkpt_times) << "\n";
        std::cout << "Restore point: " << bench_point->model_size << "," << get_median(sup_times) << "," << get_median(chkpt_times) << "\n";
        restFile << bench_point->model_size << "," << get_mean(sup_times) << "," << get_mean(chkpt_times) << "\n";
        restFile.flush();

        //clear vectors
        chkpt_times.clear();
        sup_times.clear();

        //delete checkpoint files
        //remove(NV_MODEL);
        remove(MNIST_WEIGHTS);
        printf("Benchmark progress: %d%%\n", i * 10);
    }
    //close results files
    restFile.close();
    saveFile.close();
}

//------------------------------------------------------------------------------------------------------------------------

void train_mnist(char *cfgfile)
{

#ifndef BENCHMARKING
    std::string img_path = MNIST_TRAIN_IMAGES;   //
    std::string label_path = MNIST_TRAIN_LABELS; //
    training_data = load_mnist_images(img_path); //
    training_data.y = load_mnist_labels(label_path); //
    bench_point = (bench_info *)malloc(sizeof(bench_info));
#endif

    list *sections = read_cfg(cfgfile);

    printf("Training mnist in enclave..\n");
    network *net = NULL;
    //printf("Done creating network in enclave...\n");

    srand(12345);
    float avg_loss = 0;
    float loss = 0;
    int classes = 10;
    int N = 60000; //number of training images
    int cur_batch = 0;
    float progress = 0;
    int count = 0;
    data train = training_data;
    char *path = MNIST_WEIGHTS;
    unsigned int num_params;
    bench_point->chkpt = 0;
    bench_point->sup = 0;
    //instantiate nvmodel
    NVModel *nv_net = romuluslog::RomulusLog::get_object<NVModel>(0);

    //benchmark restoration

    if (nv_net != nullptr) //we have saved weights
    {

#ifdef CRASH_TEST
        network *net = create_net_in(sections); //creates and allocates net layers
        nv_net->mirror_in(net, &avg_loss);

#else
        network *net_rest = create_net_in(sections); //creates and allocates net layers
        while (count < NUM_ITERATIONS)
        {
            count++;
            //disk restoration
            bench_point->chkpt = 1;
            bench_point->sup = 0;

            start_clock();
            load_network(net_rest, MNIST_WEIGHTS, 0);
            stop_clock();
            //free network
            //free_network(net1);

            //pm restoration
            bench_point->chkpt = 0;
            bench_point->sup = 1;
            start_clock();
            nv_net->mirror_in(net_rest, &avg_loss);
            stop_clock();

            num_params = get_param_size(net_rest);
            bench_point->model_size = (double)(num_params * 4) / (1024 * 1024);
            //point->model_size = num_params;
            printf("Number of params: %d Model size: %f\n", num_params, bench_point->model_size);
        }
        //free enclave net and pmem net
        free_network(net_rest);
        TM_PFREE(nv_net);
        romuluslog::RomulusLog::put_object<NVModel>(0, nullptr);
        return; //stop restoration benchmark
#endif
    }
    else
    {
        net = create_net_in(sections);
        printf("Initialized network in enclave...\n");
    }

    /*  num_params = get_param_size(net);
    printf("Number of params: %d\n",num_params);
    double net_size = num_params * sizeof(float)/KILO; */

    int epoch = (*net->seen) / N;
    count = 0;
    printf("Max batches: %d\n", net->max_batches);

    //benchmark model saving
    while ((cur_batch < net->max_batches || net->max_batches == 0) && count < NUM_ITERATIONS)
    {
        count++; //number of iterations for a single benchmarking point.
        cur_batch = get_current_batch(net);
        loss = train_network_sgd(net, train, 1);
        //allocate nvmodel here NB: all net attribs have been instantitated after one training iteration

        if (nv_net == nullptr) //no weights in pmem: allocate nv model
        {
            nv_net = (NVModel *)TM_PMALLOC(sizeof(struct NVModel));
            romuluslog::RomulusLog::put_object(0, nv_net);
            nv_net->allocator(net);
            avg_loss = -1; //we are training from 0 here
        }

        if (avg_loss == -1)
            avg_loss = loss;
        avg_loss = avg_loss * .95 + loss * .05;
        epoch = (*net->seen) / N;
        /*
        progress = ((double)cur_batch / net->max_batches) * 100;
        printf("Batch num: %ld, Seen: %.3f: Loss: %f, Avg loss: %f avg, L. rate: %f, Progress: %.2f%% \n",
               cur_batch, (float)(*net->seen) / N, loss, avg_loss, get_current_rate(net), progress); */

#ifdef CRASH_TEST
        if (cur_batch % 5 == 0)
        {
            bench_point->epoch = epoch;
            bench_point->loss = avg_loss;
            nv_net->mirror_out(net, &avg_loss);
            add_loss();
        }
#else
        if (cur_batch % 1 == 0)
        {
            //disk saving
            printf("Saving weights to weight file..\n");
            bench_point->chkpt = 1;
            bench_point->sup = 0;
            start_clock();
            save_weights(net, path);
            stop_clock();

            //sup mirroring
            printf("Mirroring weights to persistent memory..\n");
            bench_point->chkpt = 0;
            bench_point->sup = 1;
            start_clock();
            nv_net->mirror_out(net, &avg_loss);
            stop_clock();
        }
#endif
    }
    num_params = get_param_size(net);
    bench_point->model_size = (double)(num_params * 4) / (1024 * 1024);
    //point->model_size = num_params;
    printf("Number of params: %d Model size: %f\n", num_params, bench_point->model_size);
    printf("Done training mnist network..\n");
    free_network(net);
}

/**
 * Test a trained mnist model
 * Define path to weighfile in trainer.c
 */
void test_mnist(char *cfgfile)
{

    std::string img_path = MNIST_TEST_IMAGES;
    std::string label_path = MNIST_TEST_LABELS;
    data test = load_mnist_images(img_path);
    test.y = load_mnist_labels(label_path);
    list *sections = read_cfg(cfgfile);

    //ecall_tester(sections, &test, 0);
    printf("Mnist testing complete..\n");
    free_data(test);
}

/* Application entry */
int main(int argc, char *argv[])
{

    //Create NUM_THRREADS threads
    //std::thread trd[NUM_THREADS];
    //test_mnist(MNIST_CFG);
    /*  
    for (int i = 0; i < NUM_THREADS; i++)
    {
        trd[i] = std::thread(thread_func);
    }
    for (int i = 0; i < NUM_THREADS; i++)
    {
        trd[i].join();
    } */

    /* Benchmarking stuff */
    train_mnist(TEST_CFG);
    //run_bench();
    //crash_test();

    return 0;
}
