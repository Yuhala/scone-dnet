/*
 * Created on Thu Mar 12 2020
 *
 * Copyright (c) 2020 Peterson Yuhala, IIUN
 */

#ifndef _APP_H_
#define _APP_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "dnet_ocalls.h"
#include "data_mnist.h"
#include "dnet_sgx_utils.h"
#include "darknet.h"
#include "dnet_mirror.h"

#define CIFAR_WEIGHTS "/home/ubuntu/peterson/scone-dnet/backup/cifar.weights"
#define TINY_WEIGHTS "/home/ubuntu/peterson/scone-dnet/backup/tiny.weights"
#define MNIST_WEIGHTS "/home/ubuntu/peterson/scone-dnet/backup/mnist.weights"
#define USE_DISK
#define CRASH_TESTx

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#define NUM_THREADS 3    //number of worker threads
#define KILO 1024        //1 << 10
#define MEGA KILO * 1024 //1 << 20

//---------------------------------------------------------------------------------
/**
 * Config files
 */

#define CONFIG_BASE "/home/ubuntu/peterson/scone-dnet/cfg/big/"

#define MNIST_TRAIN_IMAGES "/home/ubuntu/peterson/scone-dnet/data/mnist/train-images-idx3-ubyte"
#define MNIST_TRAIN_LABELS "/home/ubuntu/peterson/scone-dnet/data/mnist/train-labels-idx1-ubyte"
#define MNIST_TEST_IMAGES "/home/ubuntu/peterson/scone-dnet/data/mnist/t10k-images-idx3-ubyte"
#define MNIST_TEST_LABELS "/home/ubuntu/peterson/scone-dnet/data/mnist/t10k-labels-idx1-ubyte"
#define MNIST_CFG "/home/ubuntu/peterson/scone-dnet/cfg/mnist.cfg"
#define TEST_CFG "/home/ubuntu/peterson/scone-dnet/cfg/cfg.cfg"
#define MNIST_WEIGHTS "/home/ubuntu/peterson/scone-dnet/backup/mnist.weights"
//#define NV_MODEL "/dev/shm/romuluslog_shared"
#define SAVE "/home/ubuntu/peterson/scone-dnet/results/save.csv"
#define RESTORE "/home/ubuntu/peterson/scone-dnet/results/restore.csv"
#define CRASH "/home/ubuntu/peterson/scone-dnet/results/crash.csv"
#define NOCRASH "/home/ubuntu/peterson/scone-dnet/results/nocrash.csv"
#define CRASH_TESTx

void train_cifar(char *cfgfile);
void test_cifar(char *cfgfile);
void train_mnist(char *cfgfile);
void test_mnist(char *cfgfile);

//for benchmarking purposes
void start_clock();
void stop_clock();
void add_loss();
void run_bench();
void crash_test();

#endif /* !_APP_H_ */
