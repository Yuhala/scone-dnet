/**
 * Author: Peterson Yuhala
 * Purpose: AES-256-GCM cryptography algorithms
 */

#ifndef CRYPTO_H
#define CRYPTO_H

#define IV_SIZE 12  //96 bits
#define MAC_SIZE 16 //128 bits
#define KEY_SIZE 32 //256 bits
#define ADD_ENC_DATA_SIZE (MAC_SIZE + IV_SIZE)
#define ENABLE_CRYPTOx

#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/rand.h>

typedef enum
{
    ENCRYPT, //
    DECRYPT,
    DEFAULT
} ENC_FLAG;

typedef enum
{
    GCM, //
    CTR
} AES_ALGO;

/* AES-256-GCM encryption algorithm */
void aes_gcm_encrypt(unsigned char *pt, unsigned char *ct, unsigned char *key, size_t pt_len);

/* AES-256-GCM decryption algorithm */
void aes_gcm_decrypt(unsigned char *res, unsigned char *ct, unsigned char *key, size_t ct_len);

/* Encrypted memcpy */
void enc_memcpy(void *dest, void *src, size_t n, ENC_FLAG flag);

#endif /*CRYPTO_H*/