/*
 * Created on Fri Feb 14 2020
 *
 * Copyright (c) 2020 Peterson Yuhala, IIUN
 */

#include "dnet_ocalls.h"

//File pointer used for reading/writing files from within the enclave runtime
static FILE *fp = NULL;

//encryption keys
//unsigned char *key_256 = {0x76, 0x39, 0x79, 0x24, 0x42, 0x26, 0x45, 0x28, 0x48, 0x2b, 0x4d, 0x3b, 0x62, 0x51, 0x5e, 0x8f};
unsigned char *key_128 = {0x76, 0x39, 0x79, 0x24, 0x42, 0x26, 0x45, 0x28};

// 0 for read: 1 for write
void ocall_open_file(const char *filename, flag oflag)
{
    if (!fp) //fp == NULL
    {
        switch (oflag)
        {
        case ORDONLY:
            fp = fopen(filename, "rb");
            printf("Opened file in read only mode\n");
            break;
        case OWRONLY:
            fp = fopen(filename, "wb");
            printf("Opened file in write only mode\n");
            break;
        case ORDPLUS:
            fp = fopen(filename, "r+");
            break;
        case OWRPLUS:
            fp = fopen(filename, "w+");
            break;
        default:; //nothing to do
        }
    }
    else
    {
        printf("Problem with file pointer..\n");
    }
}

/**
 * Only one file can be manipulated at any point in time
 * from within enclave. So we have no ambiguity at the level
 * of which file pointer to close..
 */
void ocall_close_file()
{
    if (fp) //fp != NULL
    {
        fclose(fp);
        fp = NULL;
    }
}

void enc_fread(void *ptr, size_t size, size_t nmemb, int dummy)
{
    
    if (fp)
    {
        //decrypt data b4 copying to pointer
        fread(ptr, size, nmemb, fp);
    }
    else
    {
        printf("Corrupt file pointer..\n");
        abort();
    }
}

void enc_fwrite(void *ptr, size_t size, size_t nmemb, int dummy)
{
    int ret;
    if (fp)
    {
        //encrypt data before writing to file
        fwrite(ptr, size, nmemb, fp);
        //make sure it is flushed to disk first
        ret = fflush(fp);
        if (ret != 0)
            printf("fflush did not work..\n");
        ret = fsync(fileno(fp));
        if (ret < 0)
            printf("fsync did not work..\n");
        return;
    }
    else
    {
        printf("Corrupt file pointer..\n");
        abort();
    }
}