// Build this with:
// g++-8 -O3 -g -DPWB_IS_CLFLUSH -I.. example1.cpp ../romuluslog/RomulusLog.cpp ../romuluslog/malloc.cpp ../common/ThreadRegistry.cpp -o example1

#include <stdio.h>
#include "../romuluslog/RomulusLog.hpp"

using namespace romuluslog;

// A simple persistent stack of integers (one integer per node)
class PStack {
    struct Node {
        persist<uint64_t> key;
        persist<Node*>    next;
    };

    persist<Node*> head {nullptr};

public:
    persist<uint64_t> EMPTY {999999999};

    void push(uint64_t key) {
        RomulusLog::updateTx([&] () {
            Node* node = RomulusLog::tmNew<Node>();
            node->next = head;
            node->key = key;
            head = node;
        });
    }

    // Returns EMPTY if the stack has no keys
    uint64_t pop(void) {
        uint64_t key = EMPTY;
        RomulusLog::updateTx([&] () {
           if (head == nullptr) return;
           Node* node = head;
           key = node->key;
           head = node->next;
           RomulusLog::tmDelete(node);
        });
        return key;
    }
};



int main(void) {
    // Create an empty stack in PM and save the root pointer (index 0) to use in a later tx
    RomulusLog::updateTx([&] () {
        printf("Creating persistent stack...\n");
        PStack* pstack = RomulusLog::tmNew<PStack>();
        RomulusLog::put_object(0, pstack);
    });

    // Add two items to the persistent stack
    RomulusLog::updateTx([&] () {
        PStack* pstack = RomulusLog::get_object<PStack>(0);
        pstack->push(33);
        pstack->push(44);
        
    });
    


    // Pop two items from the persistent stack
    RomulusLog::updateTx([&] () {
        PStack* pstack = RomulusLog::get_object<PStack>(0);
        printf("Popped two items: %ld and %ld\n", pstack->pop(), pstack->pop());
        // This one should be "EMTPY" which is 999999999
        printf("Popped one more: %ld\n", pstack->pop());
    });

    // Delete the persistent stack from persistent memory
    RomulusLog::updateTx([&] () {
        printf("Destroying persistent stack...\n");
        PStack* pstack = RomulusLog::get_object<PStack>(0);
        RomulusLog::tmDelete(pstack);
        RomulusLog::put_object<PStack>(0, nullptr);
    });
}
