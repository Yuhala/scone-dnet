/**
 * Author: Peterson Yuhala
 * Purpose: This file defines a persistent tensorflow model using romulus pmem library
 * NB: This file corresponds to a particular tf-model(linear regression model with 3 layers); the same idea
 * could easily be used for a general tf-model
 * 
 */

#ifndef NV_MODEL_H
#define NV_MODEL_H

//#include "tensorflow/core/framework/tensor.h"

#include "common.h"

//using namespace romuluslog;

/**
 * A neural network/model is stored in pmem as a linked list of layers(weights and biases)
 */

/**
 * Our heart-dnn-model has 5 layers: 1 input and 1 output layer and 3 hidden layers. 
 * We store only information of the hidden layers in pmem
 */
//TODO: add template here for param types
class NVModel
{

public:
    struct Layer
    {
        TM_TYPE<int> id;                  //Layer Id
        TM_TYPE<unsigned char *> weights; //weight param data
        TM_TYPE<unsigned char *> bias;    //bias param data
        TM_TYPE<Layer *> next;            //next layer
    };

    TM_TYPE<int> num_layers{0};

    //Epoch: training iteration afterwhich the parameters were saved
    TM_TYPE<int> epoch{0};
    TM_TYPE<Layer *> head{nullptr};

    /**
     * Update all layers in the non-volatile model using the volatile model params
     * All layers are updated in 1 romulus transaction so as to maintain a coherent model in the event of a crash within the transaction
     * @params: List of weight and bias tensors and training level/iteration
     * T1: ciphertext data type (e.g unsigned char); T2: plaintext data type (e.g float)
     */
    template <typename T1, typename T2>
    void flush_params(std::vector<Tensor> params, int level, unsigned char *key) //w(i)-->layer(i)
    {
        // name mapping between vector of Tensors and model parameters
        //     w1 --> param[0]
        //     w2 --> param[1]
        //     w3 --> param[2]
        //     b1 --> param[3]
        //     b2 --> param[4]
        //     b3 --> param[5]
        //     epoch --> level

        if (num_layers == 0)
        {
            allocate_model<T1,T2>();
        }

        int num = params.size() / 2;
        //This is a simple check: TODO: write better test to see if tensor sizes are compatible with model layer sizes.
        //Define routine CHECK_SIZE(size1,size2)--> return true if equal
        if (num_layers != num)
        {
            std::cout << "Param vector incompatible with pmem model\n";
            return;
        }

        //Encrypt params and flush layer by layer in a transaction
        /**
         * For more clarity, I save params layer by layer. Will generalize with a loop once this works
         * The weight and bias vectors contain pointers representing the 2d weight/1d bias tensors
         * All layers flushed in the same transaction to ensure a consistent model
         */

        //NB: For more clarity we do things in a somewhat repetitive fashion. For larger models with more layers loops may be used

        //a. Encrypt all params layer by layer; we do not encrypt the training epoch

        //1. Layer 1
        unsigned char pt_w_l1[W1x * W1y * sizeof(T2)]; //plaintext weight params for layer 1
        unsigned char pt_b_l1[B1 * sizeof(T2)];        //plaintext bias params for layer 1
        memcpy(pt_w_l1, params[0].flat<T2>().data(), W1x * W1y * sizeof(T2));
        memcpy(pt_b_l1, params[3].flat<T2>().data(), B1 * sizeof(T2));
        unsigned char ct_w_l1[sizeof(pt_w_l1) + ADD_ENC_DATA_SIZE]; //ciphertext weight params for layer 1
        unsigned char ct_b_l1[sizeof(pt_b_l1) + ADD_ENC_DATA_SIZE]; //ciphertext bias params for layer 1
        //DEBUG_PRINT("Size of ptw: %d Size of ctw: %d", sizeof(pt_w_l1), sizeof(ct_w_l1));
        aes_gcm_encrypt(pt_w_l1, ct_w_l1, key, sizeof(pt_w_l1));
        aes_gcm_encrypt(pt_b_l1, ct_b_l1, key, sizeof(pt_b_l1));

        //2. Layer 2
        unsigned char pt_w_l2[W2x * W2y * sizeof(T2)]; //plaintext weight params for layer 2
        unsigned char pt_b_l2[B2 * sizeof(T2)];        //plaintext bias params for layer 2
        memcpy(pt_w_l2, params[1].flat<T2>().data(), W2x * W2y * sizeof(T2));
        memcpy(pt_b_l2, params[4].flat<T2>().data(), B2 * sizeof(T2));
        unsigned char ct_w_l2[sizeof(pt_w_l2) + ADD_ENC_DATA_SIZE]; //ciphertext weight params for layer 2
        unsigned char ct_b_l2[sizeof(pt_b_l2) + ADD_ENC_DATA_SIZE]; //ciphertext bias params for layer 2
        aes_gcm_encrypt(pt_w_l2, ct_w_l2, key, sizeof(pt_w_l2));
        aes_gcm_encrypt(pt_b_l2, ct_b_l2, key, sizeof(pt_b_l2));

        //3. Layer 3
        unsigned char pt_w_l3[W3x * W3y * sizeof(T2)]; //plaintext weight params for layer 2
        unsigned char pt_b_l3[B3 * sizeof(T2)];        //plaintext bias params for layer 2
        memcpy(pt_w_l3, params[2].flat<T2>().data(), W3x * W3y * sizeof(T2));
        memcpy(pt_b_l3, params[5].flat<T2>().data(), B3 * sizeof(T2));
        unsigned char ct_w_l3[sizeof(pt_w_l3) + ADD_ENC_DATA_SIZE]; //ciphertext weight params for layer 2
        unsigned char ct_b_l3[sizeof(pt_b_l3) + ADD_ENC_DATA_SIZE]; //ciphertext bias params for layer 2
        aes_gcm_encrypt(pt_w_l3, ct_w_l3, key, sizeof(pt_w_l3));
        aes_gcm_encrypt(pt_b_l3, ct_b_l3, key, sizeof(pt_b_l3));

        //b. FLush encrypted params to pmem in a transaction
        TM_WRITE_TRANSACTION([&]() {
            //Save training level/epoch
            epoch = level;

            //1. Layer 1
            Layer *l1 = head;
            memcpy(l1->weights, ct_w_l1, sizeof(ct_w_l1)); //TODO: test with std::copy
            memcpy(l1->bias, ct_b_l1, sizeof(ct_b_l1));

            //2. Layer 2
            Layer *l2 = l1->next;
            memcpy(l2->weights, ct_w_l2, sizeof(ct_w_l2)); //TODO: test with std::copy
            memcpy(l2->bias, ct_b_l2, sizeof(ct_b_l2));

            //3. Layer 3
            Layer *l3 = l2->next;
            memcpy(l3->weights, ct_w_l3, sizeof(ct_w_l3)); //TODO: test with std::copy
            memcpy(l3->bias, ct_b_l3, sizeof(ct_b_l3));
        });
        //std::cout << "Model flushed to nvram successfully\n";
    }
    /**
      * Decrypt and Copy persistent params into model tensors
      */
    template <typename T>
    void read_params(std::vector<Tensor *> tensors, unsigned char *key)
    {

        int num = tensors.size() / 2;
        //This is a simple check: TODO: write better test to see if tensor sizes are compatible with model layer sizes.
        //Define routine CHECK_SIZE(size1,size2)--> return true if equal
        if (num_layers != num)
        {
            std::cout << "Pmem model not compatible with tensor size\n";
            return;
        }

        /**
         * Decrypt encrypted params in pmem and copy them into param tensors
         */

        //a. Decrypt encrypted params
        //1. Layer 1
        Layer *l1 = head;
        unsigned char res_w_l1[W1x * W1y * sizeof(T)]; //decrypted result: weight params for layer 1
        unsigned char res_b_l1[B1 * sizeof(T)];        //decrypted result: bias params for layer 1
        //NB: We could decrypt directly from pmem...TODO
        unsigned char ct_w_l1[sizeof(res_w_l1) + ADD_ENC_DATA_SIZE];
        unsigned char ct_b_l1[sizeof(res_b_l1) + ADD_ENC_DATA_SIZE];
        memcpy(ct_w_l1, l1->weights, sizeof(ct_w_l1));
        memcpy(ct_b_l1, l1->bias, sizeof(ct_b_l1));
        //decrypt params
        aes_gcm_decrypt(res_w_l1, ct_w_l1, key, sizeof(ct_w_l1));
        aes_gcm_decrypt(res_b_l1, ct_b_l1, key, sizeof(ct_b_l1));

        //1. Layer 2
        Layer *l2 = l1->next;
        unsigned char res_w_l2[W2x * W2y * sizeof(T)]; //decrypted result: weight params for layer 2
        unsigned char res_b_l2[B2 * sizeof(T)];        //decrypted result: bias params for layer 2
        //NB: We could decrypt directly from pmem...TODO
        unsigned char ct_w_l2[sizeof(res_w_l2) + ADD_ENC_DATA_SIZE];
        unsigned char ct_b_l2[sizeof(res_b_l2) + ADD_ENC_DATA_SIZE];        
        memcpy(ct_w_l2, l2->weights, sizeof(ct_w_l2));        
        memcpy(ct_b_l2, l2->bias, sizeof(ct_b_l2));
        
       
        //decrypt params
        aes_gcm_decrypt(res_w_l2, ct_w_l2, key, sizeof(ct_w_l2));
        aes_gcm_decrypt(res_b_l2, ct_b_l2, key, sizeof(ct_b_l2));

        //1. Layer 3
        Layer *l3 = l2->next;
        unsigned char res_w_l3[W3x * W3y * sizeof(T)]; //decrypted result: weight params for layer 3
        unsigned char res_b_l3[B3 * sizeof(T)];        //decrypted result: bias params for layer 3
        //NB: We could decrypt directly from pmem...TODO
        unsigned char ct_w_l3[sizeof(res_w_l3) + ADD_ENC_DATA_SIZE];
        unsigned char ct_b_l3[sizeof(res_b_l3) + ADD_ENC_DATA_SIZE];
        memcpy(ct_w_l3, l3->weights, sizeof(ct_w_l3));
        memcpy(ct_b_l3, l3->bias, sizeof(ct_b_l3));
        //decrypt params
        aes_gcm_decrypt(res_w_l3, ct_w_l3, key, sizeof(ct_w_l3));
        aes_gcm_decrypt(res_b_l3, ct_b_l3, key, sizeof(ct_b_l3));

        //b. Copy decrypted params into param tensors
        //1. Layer 1
        memcpy(tensors[0]->flat<T>().data(), res_w_l1, sizeof(res_w_l1));
        memcpy(tensors[3]->flat<T>().data(), res_b_l1, sizeof(res_b_l1));

        //2. Layer 2
        memcpy(tensors[1]->flat<T>().data(), res_w_l2, sizeof(res_w_l2));
        memcpy(tensors[4]->flat<T>().data(), res_b_l2, sizeof(res_b_l2));

        //3. Layer 3
        memcpy(tensors[2]->flat<T>().data(), res_w_l3, sizeof(res_w_l3));
        memcpy(tensors[5]->flat<T>().data(), res_b_l3, sizeof(res_b_l3));
    }

    /**
     * Allocate pmem for model parameters if they don't exist yet
     * NB: Template has 2 types b/c we are allocating T1 buffers for ciphertext(eg unsigned chars)
     * but plaintext is of type T2(eg float); AES-GCM encrypted block size = plaintext block size
     */
    template <typename T1, typename T2>
    void allocate_model()
    {

        int count = 0;
        /**
         * All allocations are done in a single transaction
         * Allocate and Link all the layers
         * For the sake of clarity, allocations are not done in a loop(for now) 
         * For larger models you could generalize with a loop
         */

        //NB: We encrypt data using aes-256-gcm mode and append the iv and mac to the ciphertext
        TM_WRITE_TRANSACTION([&]() {
            //Memory allocations
            Layer *l1 = (Layer *)TM_PMALLOC(sizeof(struct Layer));
            l1->id = count++;
            l1->weights = (T1 *)TM_PMALLOC(W1x * W1y * sizeof(T2) + ADD_ENC_DATA_SIZE);
            l1->bias = (T1 *)TM_PMALLOC(B1 * sizeof(T2) + ADD_ENC_DATA_SIZE);

            Layer *l2 = (Layer *)TM_PMALLOC(sizeof(struct Layer));
            l2->id = count++;
            l2->weights = (T1 *)TM_PMALLOC(W2x * W2y * sizeof(T2) + ADD_ENC_DATA_SIZE);
            l2->bias = (T1 *)TM_PMALLOC(B2 * sizeof(T2) + ADD_ENC_DATA_SIZE);

            Layer *l3 = (Layer *)TM_PMALLOC(sizeof(struct Layer));
            l3->id = count++;
            l3->weights = (T1 *)TM_PMALLOC(W3x * W3y * sizeof(T2) + ADD_ENC_DATA_SIZE);
            l3->bias = (T1 *)TM_PMALLOC(B3 * sizeof(T2) + ADD_ENC_DATA_SIZE);

            //Link the layers
            head = l1;
            l1->next = l2;
            l2->next = l3;
            l3->next = nullptr;

            num_layers = count;
        });
        std::cout << "Allocated pmem model successfully\n";
    }
};

#endif /* NV_MODEL_H */