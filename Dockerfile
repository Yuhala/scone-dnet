#
# Author: Peterson Yuhala
# Docker file to build scone-dnet project in a scone container
#

# Download base image
FROM sconecuratedimages/crosscompilers:ubuntu18.04

# Define enclave heap size
ENV SCONE_HEAP 1G
# Use SCONE crosscompiler
ENV CC scone-gcc
ENV CXX scone-g++

RUN apt-get update && apt-get install -y \
        make\
        build-essential \
        libssl-dev \
        git \
        wget         

# Create base dir for sgx-dnet-romulus
RUN mkdir -p /home/ubuntu/peterson \
        && mkdir /work

RUN cd /home/ubuntu/peterson && git clone https://gitlab.com/Yuhala/scone-dnet.git


    
#CMD ["python3"]
