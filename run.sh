#!/bin/bash

#SCONE_VERSION=1 /home/ubuntu/peterson/scone-dnet/app
results_dir=/home/ubuntu/peterson/scone-dnet/results
shm_dir=/dev/shm
back_dir=/home/ubuntu/peterson/scone-dnet/backup

sudo docker run --shm-size 1G -it --privileged \
 -v `pwd`/results:$results_dir \
 -v `pwd`/backup:$back_dir \
 -v /dev/shm:$shm_dir \
  scone-dnetv1 bash

