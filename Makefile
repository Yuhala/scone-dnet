#
# * Created on Thu Mar 12 2020
# *
# * Copyright (c) 2020 Peterson Yuhala, IIUN
#

#CXX := g++
#CC := gcc

#--------------------------------------------------------
# dnet object directories
DNET_BASE := src/dnet
SRC_BASE := src
APP_BASE := main

DNET_INC := -I$(DNET_BASE) -I$(SRC_BASE)/include


# dnet objects
OBJ := activation_layer.o activations.o avgpool_layer.o batchnorm_layer.o blas.o box.o col2im.o \
	connected_layer.o convolutional_layer.o cost_layer.o crnn_layer.o crop_layer.o deconvolutional_layer.o \
	detection_layer.o dropout_layer.o gemm.o gru_layer.o im2col.o iseg_layer.o l2norm_layer.o layer.o \
	list.o local_layer.o logistic_layer.o lstm_layer.o matrix.o maxpool_layer.o normalization_layer.o \
	region_layer.o reorg_layer.o rnn_layer.o route_layer.o shortcut_layer.o softmax_layer.o tree.o \
	upsample_layer.o utils.o image.o yolo_layer.o network.o data.o option_list.o parser.o dnet_ocalls.o data_mnist.o


DNET_OBJS := $(addprefix $(DNET_BASE)/, $(OBJ)) 

DNET_DEPS := $(wildcard $(DNET_BASE)/*.h) $(wildcard $(SRC_BASE)/include/*.h)

# romulus files
Rom_Folder := $(SRC_BASE)/romulus
Rom_Include_Paths := -I$(Rom_Folder)
Romulus_Cpp_Flags := -DPWB_IS_CLFLUSH $(Rom_Include_Paths) 
Rom_Cpp_Files := $(Rom_Folder)/romuluslog/RomulusLog.cpp $(Rom_Folder)/romuluslog/malloc.cpp $(Rom_Folder)/common/ThreadRegistry.cpp
#Rom_Cpp_Objects := $(Rom_Cpp_Files:.cpp=.o)
Rom_Obj := RomulusLog.o malloc.o ThreadRegistry.o
Rom_Cpp_Objects := $(addprefix $(Rom_Folder)/, $(Rom_Obj)) 
romulus := $(Rom_Obj)

App_Name := app

App_Cpp_Files := $(wildcard main/*.cpp) $(wildcard src/crypto/*.cpp) $(wildcard src/bench/*.cpp) $(wildcard src/mirroring/*.cpp)
App_Cpp_Objects := $(App_Cpp_Files:.cpp=.o) 
#Other_Objects := $(SRC_BASE)/bench/benchtools.o $(SRC_BASE)/crypto/crypto.o $(SRC_BASE)/mirroring/dnet_mirror.o
App_Cpp_Flags :=
APP_INC := $(DNET_INC) -I$(SRC_BASE) -I$(Rom_Folder)/romuluslog -I$(SRC_BASE)/bench -I$(SRC_BASE)/crypto -I$(SRC_BASE)/mirroring 

App_Link_Flags := -lpthread -lm -lssl -lcrypto

.PHONY: all run 

all: $(App_Name) 

$(DNET_BASE)/%.o: $(DNET_BASE)/%.c $(DNET_DEPS)
	@$(CC) $(DNET_INC) $(App_Cpp_Flags) -c $< -o $@
	@echo "CC  <=  $<"

$(DNET_BASE)/%.o: $(DNET_BASE)/%.cpp $(DNET_DEPS)
	@$(CXX) $(DNET_INC) $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(SRC_BASE)/bench/%.o: $(SRC_BASE)/bench/%.cpp $(SRC_BASE)/bench/%.h
	@$(CXX) $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(SRC_BASE)/crypto/%.o: $(SRC_BASE)/crypto/%.cpp $(SRC_BASE)/crypto/%.h
	@$(CXX) $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(SRC_BASE)/mirroring/%.o: $(SRC_BASE)/mirroring/%.cpp $(SRC_BASE)/mirroring/%.h
	@$(CXX)  $(APP_INC)  $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

#$(Rom_Folder)/%.o:$(Rom_Cpp_Files)
#	@$(CXX) $(Romulus_Cpp_Flags) -c $^

$(romulus): $(Rom_Cpp_Files)
	@$(CXX) $(Romulus_Cpp_Flags) -c $^
	@echo "CXX  <=  $<"


$(APP_BASE)/%.o: $(APP_BASE)/%.cpp
	@$(CXX) $(APP_INC) $(App_Cpp_Flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(App_Name): $(App_Cpp_Objects) $(DNET_OBJS) $(Rom_Obj) #$(Other_Objects)
	@$(CXX) $^ -o $@ $(App_Link_Flags) 
	@echo "LINK =>  $@"


.PHONY: clean

clean: 
	@rm -f $(App_Name) $(App_Cpp_Objects) $(Rom_Cpp_Objects) $(DNET_BASE)/*.o ./*.o

	






